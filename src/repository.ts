import { Action, AggregateId, EventId, Rows, Event } from './types'

type Save = <T>(a: Action<T>) => Promise<EventId>

type Get = (id: AggregateId) => Promise<Rows>

type GetAll = (event: Event) => Promise<AggregateId[]>

export type Repository = {
  save: Save
  get: Get
  getAll: GetAll
}
