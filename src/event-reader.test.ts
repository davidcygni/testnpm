import { some, none } from 'fp-ts/lib/Option'
import { Reducer, AggregateResult, eventReader, eventId } from './event-reader'
import { Row } from './types'
import { Repository } from './repository'

type Basket = {
  eggs: number
}

const basketOfEggsReducer: Reducer<Basket> = (
  acc: AggregateResult<Basket>,
  row: Row,
): AggregateResult<Basket> => {
  switch (row.event) {
    case 'BASKET_CREATED':
      return row.data
        ? some({
            eventId: eventId(row),
            data: row.data as Basket,
          })
        : none
    case 'EGG_ADDED':
      return acc.map(a => ({
        ...a,
        data: { eggs: a.data ? a.data.eggs + 1 : 0 },
      }))
    case 'EGG_CRACKED':
      throw new Error('Cracked egg exception')
    default:
      return acc
  }
}

describe('The Event Reader', () => {
  const mockGetFn = jest.fn()

  const repo: Repository = {
    get: mockGetFn,
    getAll: jest.fn(),
    save: jest.fn(),
  }

  const reader = eventReader<Basket>(repo, basketOfEggsReducer)

  it('should reduce events to build aggregate', async () => {
    const basketCreated: Row = {
      aggregateId: '123',
      event: 'BASKET_CREATED',
      currentVersion: 1,
      snapshotVersion: 0,
      data: {
        eggs: 0,
      },
    }

    const eggAdded: Row = {
      aggregateId: '123',
      event: 'EGG_ADDED',
      currentVersion: 2,
      snapshotVersion: 0,
    }

    mockGetFn.mockResolvedValue([basketCreated, eggAdded])

    const actual = await reader.get('123')
    const aggr = actual.getOrElseL(() => {
      throw new Error('test failed')
    })
    expect(aggr.data!.eggs).toEqual(1)
  })
})
