import { Pool } from 'pg'
import { Repository } from '../repository'
import { save, get, getAll } from './eventstore'
import { Action, AggregateId, Event, EventId, Rows } from '../types'

export const repository = (pool: Pool): Repository => ({
  save: <T>(action: Action<T>): Promise<EventId> => save(pool, action),
  get: (aid: AggregateId): Promise<Rows> => get(pool, aid),
  getAll: (event: Event): Promise<AggregateId[]> => getAll(pool, event),
})
