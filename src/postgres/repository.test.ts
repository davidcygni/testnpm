import { repository } from './repository'
import { Repository } from '../repository'

describe('Postgres Repository', () => {
  let queryResult: {
    rows: any[]
  }

  let pool: any

  let repo: Repository

  beforeEach(() => {
    queryResult = {
      rows: [
        {
          aggregate_id: '123-456',
          version: 1,
          snapshot_version: 0,
          event: 'My-Event',
          data: {
            'some-key': 'some-value',
          },
        },
        {
          aggregate_id: '234-567',
          version: 1,
          snapshot_version: 0,
          event: 'My-Event',
          data: {
            'some-key': 'some-value',
          },
        },
      ],
    }

    pool = {
      query: jest.fn(() => Promise.resolve(queryResult)),
    }

    repo = repository(pool)
  })

  describe('get', () => {
    const aggregateId = '123-456'

    it('should query for all rows for specific aggregate id', () => {
      repo.get(aggregateId)
      expect(pool.query).toHaveBeenCalledTimes(1)
      expect(pool.query.mock.calls[0]).toMatchSnapshot()
    })

    it('should return list', async () => {
      const result = await repo.get(aggregateId)
      expect(result).toMatchSnapshot()
    })
  })

  describe('getAll', () => {
    const event = 'My-Event'

    it('should query for all rows for specific event', () => {
      repo.getAll(event)
      expect(pool.query).toHaveBeenCalledTimes(1)
      expect(pool.query.mock.calls[0]).toMatchSnapshot()
    })

    it('should return list of only aggregate ids', async () => {
      const result = await repo.getAll(event)
      expect(result).toEqual(['123-456', '234-567'])
    })
  })
})
