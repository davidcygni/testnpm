import { QueryResult, Pool, PoolClient } from 'pg'
import {
  Action,
  AggregateId,
  Event,
  UserId,
  EventId,
  Rows,
  ErrorCategory,
} from '../types'

type Record<T> = {
  client: Pool | PoolClient
  aggregateId: AggregateId
  event: Event
  baseVersion: number
  version: number
  timestamp: number
  userId: UserId
  data: T
}

export const save = async <T>(client: Pool, a: Action<T>): Promise<EventId> => {
  try {
    const { aggregateId, event, version: baseVersion, userId, data } = a

    const version = await updateVersion(client, aggregateId).then(
      rs => rs.rows[0].version,
    )

    return commit({
      client,
      aggregateId,
      event,
      baseVersion,
      version,
      timestamp: Date.now(),
      userId,
      data,
    }).then((qr: QueryResult) => {
      const row = qr.rows[0]
      return {
        aggregateId: row['aggregate_id'],
        currentVersion: row['version'],
        snapshotVersion: row['snapshot_version'],
      }
    })
  } catch (error) {
    return Promise.reject<EventId>(error)
  }
}

export const get = (pool: Pool, aid: AggregateId): Promise<Rows> =>
  pool
    .query(
      'SELECT * FROM "commits" WHERE "aggregate_id" = $1 ORDER BY "sequence_nr"',
      [aid],
    )
    .then((qr: QueryResult) =>
      qr.rows.map(row => ({
        aggregateId: row['aggregate_id'],
        currentVersion: row['version'],
        snapshotVersion: row['snapshot_version'],
        event: row['event'],
        data: row['data'],
      })),
    )

export const getAll = async (
  pool: Pool,
  event: Event,
): Promise<AggregateId[]> =>
  pool
    .query(
      'SELECT * FROM "commits" WHERE "event" = $1 ORDER BY "sequence_nr" DESC',
      [event],
    )
    .then((qr: QueryResult) => qr.rows.map(row => row['aggregate_id']))

const updateVersion = (
  client: Pool | PoolClient,
  id: AggregateId,
): Promise<QueryResult> =>
  client.query(
    `INSERT INTO "versions"("aggregate_id") VALUES($1)
ON CONFLICT ("aggregate_id") 
DO
 UPDATE
   SET version = versions.version + 1
RETURNING versions.version`,
    [id],
  )

const commit = <T>(r: Record<T>): Promise<QueryResult> =>
  r.client
    .query(
      `INSERT INTO "commits" ("aggregate_id", "event", "base_version", "version", "timestamp", "user_id", "data")
VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING "aggregate_id", "version", "snapshot_version"`,
      [
        r.aggregateId,
        r.event,
        r.baseVersion,
        r.version,
        r.timestamp,
        r.userId,
        r.data,
      ],
    )
    .catch(err => {
      if (err.code === '23505') {
        err.category = ErrorCategory.ConcurrencyError
        throw err
      } else {
        throw err
      }
    })
