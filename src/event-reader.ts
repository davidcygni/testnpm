import { Option, none } from 'fp-ts/lib/Option'
import { Aggregate, AggregateId, Row, EventId, Event } from './types'
import { Repository } from './repository'

export type AggregateResult<T> = Option<Aggregate<T>>

export type Reducer<T> = (
  acc: AggregateResult<T>,
  row: Row,
) => AggregateResult<T>

export type EventReader<T> = {
  get: (id: AggregateId) => Promise<AggregateResult<T>>
  getAll: (event: Event) => Promise<AggregateId[]>
}

export type EventReaderFactory = <T>(
  rep: Repository,
  red: Reducer<T>,
) => EventReader<T>

export const eventReader: EventReaderFactory = <T>(
  rep: Repository,
  red: Reducer<T>,
): EventReader<T> => ({
  get: (id: AggregateId): Promise<AggregateResult<T>> => get(id, rep, red),
  getAll: (event: Event): Promise<AggregateId[]> => getAll(event, rep),
})

export const eventId = (row: Row): EventId => ({
  aggregateId: row.aggregateId,
  currentVersion: row.currentVersion,
  snapshotVersion: row.snapshotVersion,
})

const get = <T>(
  id: AggregateId,
  rep: Repository,
  red: Reducer<T>,
): Promise<AggregateResult<T>> =>
  rep.get(id).then(rows => rows.reduce(red, none))

const getAll = (event: Event, rep: Repository): Promise<AggregateId[]> =>
  rep.getAll(event)
