export type AggregateId = string

export type Event = string

export type UserId = string

export type Action<T> = {
  aggregateId: AggregateId
  event: Event
  version: number
  userId: UserId
  data?: T
}

export type EventId = {
  aggregateId: AggregateId
  currentVersion: number
  snapshotVersion: number
}

export type Record<T> = {
  aggregate_id: AggregateId // eslint-disable-line camelcase
  event: Event
  version: number
  snapshot_version: number // eslint-disable-line camelcase
  data?: T
}

export type Aggregate<T> = {
  eventId: EventId
  data?: T
}

export type Row = {
  aggregateId: string
  currentVersion: number
  snapshotVersion: number
  event: string
  data?: object
}

export type Rows = Row[]

export const ErrorCategory = {
  ConcurrencyError: 'CONCURRENCY_ERROR',
}
