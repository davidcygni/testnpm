import { Pool } from 'pg';
import { Action, EventId } from '../types';
export declare const save: <T>(client: Pool, a: Action<T>) => Promise<EventId>;
export declare const get: (pool: Pool, aid: string) => Promise<import("../types").Row[]>;
export declare const getAll: (pool: Pool, event: string) => Promise<string[]>;
