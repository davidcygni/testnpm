import { Pool } from 'pg';
import { Repository } from '../repository';
export declare const repository: (pool: Pool) => Repository;
