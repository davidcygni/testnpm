"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const eventstore_1 = require("./eventstore");
exports.repository = (pool) => ({
    save: (action) => eventstore_1.save(pool, action),
    get: (aid) => eventstore_1.get(pool, aid),
    getAll: (event) => eventstore_1.getAll(pool, event),
});
