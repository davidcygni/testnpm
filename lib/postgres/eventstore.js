"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
exports.save = (client, a) => __awaiter(this, void 0, void 0, function* () {
    try {
        const { aggregateId, event, version: baseVersion, userId, data } = a;
        const version = yield updateVersion(client, aggregateId).then(rs => rs.rows[0].version);
        return commit({
            client,
            aggregateId,
            event,
            baseVersion,
            version,
            timestamp: Date.now(),
            userId,
            data,
        }).then((qr) => {
            const row = qr.rows[0];
            return {
                aggregateId: row['aggregate_id'],
                currentVersion: row['version'],
                snapshotVersion: row['snapshot_version'],
            };
        });
    }
    catch (error) {
        return Promise.reject(error);
    }
});
exports.get = (pool, aid) => pool
    .query('SELECT * FROM "commits" WHERE "aggregate_id" = $1 ORDER BY "sequence_nr"', [aid])
    .then((qr) => qr.rows.map(row => ({
    aggregateId: row['aggregate_id'],
    currentVersion: row['version'],
    snapshotVersion: row['snapshot_version'],
    event: row['event'],
    data: row['data'],
})));
exports.getAll = (pool, event) => __awaiter(this, void 0, void 0, function* () {
    return pool
        .query('SELECT * FROM "commits" WHERE "event" = $1 ORDER BY "sequence_nr" DESC', [event])
        .then((qr) => qr.rows.map(row => row['aggregate_id']));
});
const updateVersion = (client, id) => client.query(`INSERT INTO "versions"("aggregate_id") VALUES($1)
ON CONFLICT ("aggregate_id") 
DO
 UPDATE
   SET version = versions.version + 1
RETURNING versions.version`, [id]);
const commit = (r) => r.client
    .query(`INSERT INTO "commits" ("aggregate_id", "event", "base_version", "version", "timestamp", "user_id", "data")
VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING "aggregate_id", "version", "snapshot_version"`, [
    r.aggregateId,
    r.event,
    r.baseVersion,
    r.version,
    r.timestamp,
    r.userId,
    r.data,
])
    .catch(err => {
    if (err.code === '23505') {
        err.category = types_1.ErrorCategory.ConcurrencyError;
        throw err;
    }
    else {
        throw err;
    }
});
