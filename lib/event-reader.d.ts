import { Option } from 'fp-ts/lib/Option';
import { Aggregate, AggregateId, Row, EventId, Event } from './types';
import { Repository } from './repository';
export declare type AggregateResult<T> = Option<Aggregate<T>>;
export declare type Reducer<T> = (acc: AggregateResult<T>, row: Row) => AggregateResult<T>;
export declare type EventReader<T> = {
    get: (id: AggregateId) => Promise<AggregateResult<T>>;
    getAll: (event: Event) => Promise<AggregateId[]>;
};
export declare type EventReaderFactory = <T>(rep: Repository, red: Reducer<T>) => EventReader<T>;
export declare const eventReader: EventReaderFactory;
export declare const eventId: (row: Row) => EventId;
