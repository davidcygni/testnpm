export declare type AggregateId = string;
export declare type Event = string;
export declare type UserId = string;
export declare type Action<T> = {
    aggregateId: AggregateId;
    event: Event;
    version: number;
    userId: UserId;
    data?: T;
};
export declare type EventId = {
    aggregateId: AggregateId;
    currentVersion: number;
    snapshotVersion: number;
};
export declare type Record<T> = {
    aggregate_id: AggregateId;
    event: Event;
    version: number;
    snapshot_version: number;
    data?: T;
};
export declare type Aggregate<T> = {
    eventId: EventId;
    data?: T;
};
export declare type Row = {
    aggregateId: string;
    currentVersion: number;
    snapshotVersion: number;
    event: string;
    data?: object;
};
export declare type Rows = Row[];
export declare const ErrorCategory: {
    ConcurrencyError: string;
};
