import { Action, AggregateId, EventId, Rows, Event } from './types';
declare type Save = <T>(a: Action<T>) => Promise<EventId>;
declare type Get = (id: AggregateId) => Promise<Rows>;
declare type GetAll = (event: Event) => Promise<AggregateId[]>;
export declare type Repository = {
    save: Save;
    get: Get;
    getAll: GetAll;
};
export {};
