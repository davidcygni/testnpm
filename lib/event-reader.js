"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Option_1 = require("fp-ts/lib/Option");
exports.eventReader = (rep, red) => ({
    get: (id) => get(id, rep, red),
    getAll: (event) => getAll(event, rep),
});
exports.eventId = (row) => ({
    aggregateId: row.aggregateId,
    currentVersion: row.currentVersion,
    snapshotVersion: row.snapshotVersion,
});
const get = (id, rep, red) => rep.get(id).then(rows => rows.reduce(red, Option_1.none));
const getAll = (event, rep) => rep.getAll(event);
