# BDSN Event Store

This is a small library that handles all connections to the BDSN Eventstore.

## Semantic Versioning

This library is using semantic versioning to fit into the Node echo system. This means that is versioned on the form __Major.Minor.Patch__ where __Major__, __Minor__ and __Patch__ are integers.

- The __Major__ version is only bumped when backwards compatibility is broken.
- The __Minor__ version is bumped when there is a new feature.
- The __Patch__ version is bumped when the code is changed (refactorings, bug fixes, etc) but there are new new features.

### Setting the version

The versioning is handled (almost) automatically by the build pipeline. All you have to do as a developer is to prefix your commit comment with `major` or `breaking` for bumping a __Major__ version, `feature` or `minor` for bumping a __Minor__ version and `fix` or `patch` for pumping the __Patch__ version.

This only happens on the master branch though, so you may do as many builds as you have to on feature branches without bumping the version. The automatic versioning kicks in first when a successful build is merged to master (or when a commit is pushed on master).

#### Examples
- Johan is fixing a bug and refactoring some code. When he is satisfied with the changes he has made, he adds all the changes to his local branch as usual by doing a `git add .`. When he commits the changes he prefixes the message with `fix` or `patch` like so `git commit -m"fix that anyoing bug with the logger and refactor some tests"`.
- Hilda is adding a new function to the library. When she is finished, she adds all the changes to her local branch, `git add .`, followed by a commit where the message is prefixed by `feature` or `minor`, `git commit -m"feature: remove entities function"`.
- David is refactoring the interface of the library. When is is done, he adds all changes to his local branch, `git add .`, followed by a commit where the message is prefixed by `breaking` or `major`, `git commit -m"major: Refactored public interface"`.
