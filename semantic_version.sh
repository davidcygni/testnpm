#!/usr/bin/env bash
VERSION=`git describe --tags --match "[0-9]\.[0-9]\.[0-9]" 2>/dev/null $(git rev-list --tags --max-count=1)`

VERSION=`echo $VERSION | cut -d '-' -f 1`

if [ -z "$VERSION" ]; then
  VERSION='0.0.0'
fi

VERSION_BITS=(${VERSION//./ })

MAJOR=${VERSION_BITS[0]}
MINOR=${VERSION_BITS[1]}
PATCH=${VERSION_BITS[2]}

CMD_MAJOR=`git log -1 --pretty=%B | egrep -c '^(breaking|major)'`
CMD_MINOR=`git log -1 --pretty=%B | egrep -c '^(feature|minor)'`
CMD_PATCH=`git log -1 --pretty=%B | egrep -c '^(fix|patch)'`

yarn config set version-tag-prefix "" 

if [ $CMD_MAJOR -gt 0 ]; then
  yarn version --major
fi

if [ $CMD_MINOR -gt 0 ]; then
  yarn version --minor
fi

if [ $CMD_PATCH -gt 0 ]; then
  yarn version --patch
fi

git push; git push origin --tags
